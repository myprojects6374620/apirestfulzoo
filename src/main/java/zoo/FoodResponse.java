package zoo;
import zoo.model.Food;

import java.util.List;

public class FoodResponse {
    private Food food;
    private List<Link> links;

    public FoodResponse() {
    }

    public FoodResponse(Food food, List<Link> links) {
        this.food = food;
        this.links = links;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
