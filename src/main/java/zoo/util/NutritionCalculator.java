package zoo.util;

import zoo.IFood;
import zoo.model.FeedingPlan;
import zoo.model.Food;
import zoo.model.Nutrition;
import zoo.model.NutritionRequirement;

public class NutritionCalculator {
    public static Nutrition calculateNutritionalValue(FeedingPlan feedingPlan) {
        int totalCalories = 0;
        double totalProtein = 0;
        double totalFat = 0;

        for (IFood food : feedingPlan.getMeals()) {
            Nutrition nutritionalValue = food.getNutritionalValue();
            totalCalories += nutritionalValue.getCalories();
            totalProtein += nutritionalValue.getProtein();
            totalFat += nutritionalValue.getFat();
        }

        return new Nutrition(totalCalories, totalProtein, totalFat);
    }

    public static boolean verifyNutritionalRequirements(FeedingPlan feedingPlan, NutritionRequirement requirement) {
        Nutrition nutritionalValue = calculateNutritionalValue(feedingPlan);

        return nutritionalValue.getCalories() >= requirement.getMinCalories() &&
                nutritionalValue.getCalories() <= requirement.getMaxCalories() &&
                nutritionalValue.getProtein() >= requirement.getMinProtein() &&
                nutritionalValue.getFat() >= requirement.getMinFat() &&
                nutritionalValue.getFat() <= requirement.getMaxFat();
    }
}