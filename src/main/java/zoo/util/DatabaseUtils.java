package zoo.util;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DatabaseUtils {
    private static final String DB_URL = "jdbc:h2:file:~/test;DB_CLOSE_DELAY=-1";
    private static final String DB_DRIVER = "org.h2.Driver";

    private static Connection connection;

    static {
        try {
            Class.forName(DB_DRIVER);
            Connection conn = DriverManager.getConnection(DB_URL);
            Statement statement = conn.createStatement();
            statement.execute(new Scanner(DatabaseUtils.class.getResourceAsStream("/create_tables.sql")).useDelimiter("\\A").next());
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            connection = DriverManager.getConnection(DB_URL);
        }
        return connection;
    }
}