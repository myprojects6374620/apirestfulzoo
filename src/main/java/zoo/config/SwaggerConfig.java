package zoo.config;

import io.swagger.v3.jaxrs2.integration.JaxrsApplicationAndAnnotationScanner;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/api")
public class SwaggerConfig extends ResourceConfig {
    public SwaggerConfig() {
        packages("com.example.yourpackage"); // Paquete donde están tus recursos JAX-RS
        register(OpenApiResource.class);
        register(new JaxrsApplicationAndAnnotationScanner());
    }
}
