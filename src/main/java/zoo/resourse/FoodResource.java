package zoo.resourse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import zoo.FoodResponse;
import zoo.Link;
import zoo.dao.FoodDao;
import zoo.model.Food;
import zoo.model.Nutrition;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Path("/foods")
public class FoodResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get All Foods", description = "Fetch all foods")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation", content = @Content(schema = @Schema(implementation = Food.class))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public Response getAllFoods() {
        try {
            FoodDao foodDao = new FoodDao();
            List<Food> foods = foodDao.getAllFoods();
            List<FoodResponse> foodResponses = new ArrayList<>();

            for (Food food : foods) {
                List<Link> links = new ArrayList<>();
                links.add(new Link("/foods/" + food.getId(), "self"));
                links.add(new Link("/foods", "allFoods"));
                foodResponses.add(new FoodResponse(food, links));
            }

            return Response.ok(foodResponses, MediaType.APPLICATION_JSON).build();
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get Food By ID", description = "Fetch a food item by its ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation", content = @Content(schema = @Schema(implementation = Food.class))),
            @ApiResponse(responseCode = "404", description = "Food Not Found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public Response getFoodById(@PathParam("id") String id) {
        try {
            FoodDao foodDao = new FoodDao();
            Food food = foodDao.getFoodById(id);
            if (food != null) {
                List<Link> links = new ArrayList<>();
                links.add(new Link("/foods/" + food.getId(), "self"));
                links.add(new Link("/foods", "allFoods"));
                FoodResponse foodResponse = new FoodResponse(food, links);
                return Response.ok(foodResponse, MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create Food", description = "Create a new food item")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Food Created", content = @Content(schema = @Schema(implementation = Food.class))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public Response createFood(Food food) {
        try {
            FoodDao foodDao = new FoodDao();
            String id = UUID.randomUUID().toString();
            food.setId(id);
            foodDao.createFood(food);
            return Response.status(Response.Status.CREATED).entity(food).build();
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update Food", description = "Update an existing food item")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Food Updated", content = @Content(schema = @Schema(implementation = Food.class))),
            @ApiResponse(responseCode = "404", description = "Food Not Found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public Response updateFood(@PathParam("id") String id, Food updatedFood) {
        try {
            FoodDao foodDao = new FoodDao();
            boolean updated = foodDao.updateFood(id, updatedFood);
            if (updated) {
                return Response.ok(updatedFood, MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Operation(summary = "Delete Food", description = "Delete an existing food item")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Food Deleted"),
            @ApiResponse(responseCode = "404", description = "Food Not Found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public Response deleteFood(@PathParam("id") String id) {
        try {
            FoodDao foodDao = new FoodDao();
            boolean deleted = foodDao.deleteFood(id);
            if (deleted) {
                return Response.status(Response.Status.NO_CONTENT).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("/{foodId}/nutrition")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get Food Nutrition", description = "Fetch the nutritional value of a food item by its ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation", content = @Content(schema = @Schema(implementation = Nutrition.class))),
            @ApiResponse(responseCode = "404", description = "Food Not Found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public Response getFoodNutrition(@PathParam("foodId") String foodId) {
        try {
            FoodDao foodDao = new FoodDao();
            Food food = foodDao.getFoodById(foodId);
            if (food != null) {
                Nutrition nutritionalValue = food.getNutritionalValue();
                return Response.ok(nutritionalValue, MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
