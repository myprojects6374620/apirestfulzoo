package zoo.resourse;

import java.util.List;

public class PaginatedResponse<T> {
    private List<T> data;
    private int page;
    private int size;
    private int totalPages;

    public PaginatedResponse(List<T> data, int page, int size, int totalPages) {
        this.data = data;
        this.page = page;
        this.size = size;
        this.totalPages = totalPages;
    }

    public List<T> getData() {
        return data;
    }

    public int getPage() {
        return page;
    }

    public int getSize() {
        return size;
    }

    public int getTotalPages() {
        return totalPages;
    }
}