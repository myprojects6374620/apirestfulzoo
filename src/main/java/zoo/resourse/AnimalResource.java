package zoo.resourse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import zoo.AnimalResponse;
import zoo.Link;
import zoo.dao.AnimalDAO;
import zoo.model.Animal;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Path("/animals")
public class AnimalResource {

    private List<Animal> animals = new ArrayList<>();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get All Animals", description = "Fetch all animals with pagination")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation", content = @Content(schema = @Schema(implementation = PaginatedResponse.class))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public PaginatedResponse<AnimalResponse> getAllAnimals(@QueryParam("page") @DefaultValue("1") int page,
                                                           @QueryParam("size") @DefaultValue("10") int size) {
        try {
            AnimalDAO animalDAO = new AnimalDAO();
            List<Animal> animals = animalDAO.getAllAnimals(page, size);
            List<AnimalResponse> animalResponses = new ArrayList<>();

            for (Animal animal : animals) {
                List<Link> links = new ArrayList<>();
                links.add(new Link("/animals/" + animal.getId(), "self"));
                links.add(new Link("/animals", "allAnimals"));
                animalResponses.add(new AnimalResponse(animal, links));
            }

            int totalAnimals = animalDAO.getTotalAnimalsCount();
            int totalPages = (int) Math.ceil((double) totalAnimals / size);
            return new PaginatedResponse<>(animalResponses, page, size, totalPages);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create Animal", description = "Create a new animal")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Animal Created", content = @Content(schema = @Schema(implementation = Animal.class))),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public Response createAnimal(Animal animal) {
        try {
            AnimalDAO animalDAO = new AnimalDAO();
            String id = UUID.randomUUID().toString();
            animal.setId(id);
            animalDAO.createAnimal(animal);
            return Response.status(Response.Status.CREATED).entity(animal).build();
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get Animal By ID", description = "Fetch an animal by its ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation", content = @Content(schema = @Schema(implementation = Animal.class))),
            @ApiResponse(responseCode = "404", description = "Animal Not Found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public Response getAnimalById(@PathParam("id") String id) {
        try {
            AnimalDAO animalDAO = new AnimalDAO();
            Animal animal = animalDAO.getAnimalById(id);
            if (animal != null) {
                List<Link> links = new ArrayList<>();
                links.add(new Link("/animals/" + animal.getId(), "self"));
                links.add(new Link("/animals", "allAnimals"));
                AnimalResponse animalResponse = new AnimalResponse(animal, links);
                return Response.ok(animalResponse, MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update Animal", description = "Update an existing animal")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Animal Updated", content = @Content(schema = @Schema(implementation = Animal.class))),
            @ApiResponse(responseCode = "404", description = "Animal Not Found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public Response updateAnimal(@PathParam("id") String id, Animal updatedAnimal) {
        try {
            AnimalDAO animalDAO = new AnimalDAO();
            boolean updated = animalDAO.updateAnimal(id, updatedAnimal);
            if (updated) {
                return Response.ok(updatedAnimal, MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("/{id}")
    @Operation(summary = "Delete Animal", description = "Delete an existing animal")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Animal Deleted"),
            @ApiResponse(responseCode = "404", description = "Animal Not Found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error")
    })
    public Response deleteAnimal(@PathParam("id") String id) {
        try {
            AnimalDAO animalDAO = new AnimalDAO();
            boolean deleted = animalDAO.deleteAnimal(id);
            if (deleted) {
                return Response.status(Response.Status.NO_CONTENT).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
