package zoo.resourse;

import zoo.IFood;
import zoo.dao.AnimalDAO;
import zoo.dao.FeedingPlanDAO;
import zoo.model.Animal;
import zoo.model.FeedingPlan;
import zoo.model.Food;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;



@Path("/animals/{animalId}/feedingPlans")
public class FeedingPlanResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(summary = "Get Feeding Plans", description = "Fetch all feeding plans for a specific animal")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Successful Operation", content = @Content(schema = @Schema(implementation = FeedingPlan.class))),
            @ApiResponse(responseCode = "404", description = "Feeding Plans Not Found")
    })
    public Response getFeedingPlan(@PathParam("animalId") String animalId, @QueryParam("page") int page, @QueryParam("size") int size) {
        try {
            FeedingPlanDAO feedingPlanDAO = new FeedingPlanDAO();
            List<FeedingPlan> feedingPlans = feedingPlanDAO.getAllFeedingPlans(page, size);
            if (feedingPlans != null && !feedingPlans.isEmpty()) {
                return Response.ok(feedingPlans, MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Create Feeding Plan", description = "Create a new feeding plan for a specific animal")
    @ApiResponses({
            @ApiResponse(responseCode = "201", description = "Feeding Plan Created", content = @Content(schema = @Schema(implementation = FeedingPlan.class))),
            @ApiResponse(responseCode = "404", description = "Animal Not Found")
    })
    public Response createFeedingPlan(@PathParam("animalId") String animalId, List<Food> meals) {
        try {
            AnimalDAO animalDAO = new AnimalDAO();
            Animal animal = animalDAO.getAnimalById(animalId);
            if (animal != null) {
                FeedingPlanDAO feedingPlanDAO = new FeedingPlanDAO();
                String id = UUID.randomUUID().toString();
                FeedingPlan feedingPlan = new FeedingPlan();
                feedingPlan.setId(id);
                feedingPlan.setAnimal(animal);
                for (Food food : meals) {
                    feedingPlan.addMeal((IFood) food);
                }
                feedingPlanDAO.createFeedingPlan(feedingPlan);
                return Response.status(Response.Status.CREATED).entity(feedingPlan).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Update Feeding Plan", description = "Update an existing feeding plan for a specific animal")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Feeding Plan Updated", content = @Content(schema = @Schema(implementation = FeedingPlan.class))),
            @ApiResponse(responseCode = "404", description = "Feeding Plan or Animal Not Found")
    })
    public Response updateFeedingPlan(@PathParam("animalId") String animalId, FeedingPlan updatedFeedingPlan) {
        try {
            AnimalDAO animalDAO = new AnimalDAO();
            Animal animal = animalDAO.getAnimalById(animalId);
            if (animal != null) {
                FeedingPlanDAO feedingPlanDAO = new FeedingPlanDAO();
                updatedFeedingPlan.setAnimal(animal);
                boolean updated = feedingPlanDAO.updateFeedingPlan(updatedFeedingPlan);
                if (updated) {
                    return Response.ok(updatedFeedingPlan, MediaType.APPLICATION_JSON).build();
                } else {
                    return Response.status(Response.Status.NOT_FOUND).build();
                }
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DELETE
    @Path("/{feedingPlanId}")
    @Operation(summary = "Delete Feeding Plan", description = "Delete an existing feeding plan for a specific animal")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "Feeding Plan Deleted"),
            @ApiResponse(responseCode = "404", description = "Feeding Plan Not Found")
    })
    public Response deleteFeedingPlan(@PathParam("animalId") String animalId, @PathParam("feedingPlanId") String feedingPlanId) {
        try {
            FeedingPlanDAO feedingPlanDAO = new FeedingPlanDAO();
            boolean deleted = feedingPlanDAO.deleteFeedingPlan(feedingPlanId);
            if (deleted) {
                return Response.status(Response.Status.NO_CONTENT).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
