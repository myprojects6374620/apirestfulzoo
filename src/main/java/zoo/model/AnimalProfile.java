package zoo.model;

import zoo.IAnimal;
import zoo.IAnimalProfile;

public class AnimalProfile implements IAnimalProfile {
    private Animal animal;
    private String dietaryNeeds;
    private String medicalHistory;

    public AnimalProfile(Animal animal, String dietaryNeeds, String medicalHistory) {
        this.animal = animal;
        this.dietaryNeeds = dietaryNeeds;
        this.medicalHistory = medicalHistory;
    }

    @Override
    public IAnimal getAnimal() {
        return (IAnimal) animal;
    }

    @Override
    public String getDietaryNeeds() {
        return null;
    }

    @Override
    public String getMedicalHistory() {
        return null;
    }

    @Override
    public void setDietaryNeeds(String dietaryNeeds) {

    }

    @Override
    public void setMedicalHistory(String medicalHistory) {

    }

    // Implement all other interface methods (getters, setters, etc.)
}