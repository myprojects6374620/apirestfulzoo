package zoo.model;

public class NutritionRequirement {
    private String id;
    private int minCalories;
    private int maxCalories;
    private double minProtein;
    private double minFat;
    private double maxFat;

    public NutritionRequirement() {
    }

    public NutritionRequirement(int minCalories, int maxCalories, double minProtein, double minFat, double maxFat) {
        this.minCalories = minCalories;
        this.maxCalories = maxCalories;
        this.minProtein = minProtein;
        this.minFat = minFat;
        this.maxFat = maxFat;
    }

    // Getters y setters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMinCalories() {
        return minCalories;
    }

    public void setMinCalories(int minCalories) {
        this.minCalories = minCalories;
    }

    public int getMaxCalories() {
        return maxCalories;
    }

    public void setMaxCalories(int maxCalories) {
        this.maxCalories = maxCalories;
    }

    public double getMinProtein() {
        return minProtein;
    }

    public void setMinProtein(double minProtein) {
        this.minProtein = minProtein;
    }

    public double getMinFat() {
        return minFat;
    }

    public void setMinFat(double minFat) {
        this.minFat = minFat;
    }

    public double getMaxFat() {
        return maxFat;
    }

    public void setMaxFat(double maxFat) {
        this.maxFat = maxFat;
    }
}
