package zoo.model;

public class Nutrition {
    private int calories;
    private double protein;
    private double fat;

    public Nutrition() {
        // Constructor vacío
    }

    public Nutrition(int calories, double protein, double fat) {
        this.calories = calories;
        this.protein = protein;
        this.fat = fat;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public double getProtein() {
        return protein;
    }

    public void setProtein(double protein) {
        this.protein = protein;
    }

    public double getFat() {
        return fat;
    }

    public void setFat(double fat) {
        this.fat = fat;
    }

    public void add(Nutrition nutritionalValue) {
        this.calories += nutritionalValue.getCalories();
        this.protein += nutritionalValue.getProtein();
        this.fat += nutritionalValue.getFat();
    }
}