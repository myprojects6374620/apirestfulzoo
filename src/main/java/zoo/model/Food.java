package zoo.model;

public class    Food {
    private String id;
    private String name;
    private String foodType;
    private Nutrition nutritionalValue;

    public Food() {
    }

    public Food(String id, String name, String foodType, Nutrition nutritionalValue) {
        this.id = id;
        this.name = name;
        this.foodType = foodType;
        this.nutritionalValue = nutritionalValue;
    }

    // Getters y setters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public Nutrition getNutritionalValue() {
        return nutritionalValue;
    }

    public void setNutritionalValue(Nutrition nutritionalValue) {
        this.nutritionalValue = nutritionalValue;
    }
}