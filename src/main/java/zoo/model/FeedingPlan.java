package zoo.model;

import zoo.IAnimal;
import zoo.IFood;
import zoo.IFeedingPlan;

import java.util.ArrayList;
import java.util.List;

public class FeedingPlan implements IFeedingPlan {
    private String id;
    private IAnimal animal;
    private List<IFood> meals;

    public FeedingPlan() {
        this.meals = new ArrayList<>();
    }

    public FeedingPlan(IAnimal animal) {
        this.animal = animal;
        this.meals = new ArrayList<>();
    }

    @Override
    public IAnimal getAnimal() {
        return animal;
    }

    @Override
    public List<IFood> getMeals() {
        return meals;
    }

    @Override
    public void addMeal(IFood food) {
        meals.add(food);
    }

    @Override
    public void removeMeal(IFood food) {
        meals.remove(food);
    }

    @Override
    public Nutrition calculateNutritionalValue() {
        // Implementar el cálculo del valor nutricional basado en las comidas
        return null;
    }

    @Override
    public boolean verifyNutritionalRequirements() {
        // Implementar la verificación de los requisitos nutricionales basados en el perfil del animal
        return true;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void setAnimal(Animal animal) {
        this.animal = animal;
    }
}