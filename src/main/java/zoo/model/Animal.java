package zoo.model;

import zoo.IAnimal;
import zoo.IAnimalProfile;

public class Animal implements IAnimal {
    private String id;
    private String name;
    private String species;
    private int age;
    private String sex;
    private double weight;
    private String healthStatus;
    private IAnimalProfile profile;

    // Constructor sin argumentos
    public Animal() {
    }

    public Animal(String name, String species, int age, String sex, double weight, String healthStatus) {
        this.name = name;
        this.species = species;
        this.age = age;
        this.sex = sex;
        this.weight = weight;
        this.healthStatus = healthStatus;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSpecies() {
        return species;
    }

    @Override
    public int getAge() {
        return age;
    }

    @Override
    public String getSex() {
        return sex;
    }

    @Override
    public double getWeight() {
        return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void setHealthStatus(String healthStatus) {
        this.healthStatus = healthStatus;
    }

    @Override
    public String getHealthStatus() {
        return healthStatus;
    }

    // Implement all other interface methods (getters, setters, etc.)

    @Override
    public IAnimalProfile getProfile() {
        return profile;
    }

    @Override
    public void setProfile(IAnimalProfile profile) {
        this.profile = profile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}