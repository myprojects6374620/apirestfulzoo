package zoo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/mirecurso")
public class MiRecurso {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hola() {
        return "¡Hola, esta es mi primera API REST!";
    }
}