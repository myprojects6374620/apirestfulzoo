package zoo.dao;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import zoo.model.FeedingPlan;
import zoo.util.DatabaseUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FeedingPlanDAO {

    private final Cache<String, List<FeedingPlan>> feedingPlansCache;

    public FeedingPlanDAO() {
        feedingPlansCache = CacheBuilder.newBuilder()
                .expireAfterWrite(10, TimeUnit.MINUTES)
                .maximumSize(100)
                .build();
    }

    public void createFeedingPlan(FeedingPlan feedingPlan) throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "INSERT INTO FeedingPlan (id, animal_id) VALUES (?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, feedingPlan.getId());
        statement.setString(2, feedingPlan.getAnimal().getId());
        statement.executeUpdate();
    }

    public List<FeedingPlan> getAllFeedingPlans(int page, int size) throws SQLException {
        String cacheKey = "page_" + page + "_size_" + size;
        List<FeedingPlan> cachedFeedingPlans = feedingPlansCache.getIfPresent(cacheKey);
        if (cachedFeedingPlans != null) {
            return cachedFeedingPlans;
        }

        List<FeedingPlan> feedingPlans = new ArrayList<>();
        Connection connection = DatabaseUtils.getConnection();
        String sql = "SELECT * FROM FeedingPlan LIMIT ? OFFSET ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, size);
        statement.setInt(2, (page - 1) * size);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            FeedingPlan feedingPlan = new FeedingPlan();
            feedingPlan.setId(resultSet.getString("id"));
            AnimalDAO animalDAO = new AnimalDAO();
            feedingPlan.setAnimal(animalDAO.getAnimalById(resultSet.getString("animal_id")));
            feedingPlans.add(feedingPlan);
        }

        feedingPlansCache.put(cacheKey, feedingPlans);
        return feedingPlans;
    }

    public FeedingPlan getFeedingPlanByAnimalId(String id) throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "SELECT * FROM FeedingPlan WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            FeedingPlan feedingPlan = new FeedingPlan();
            feedingPlan.setId(resultSet.getString("id"));
            AnimalDAO animalDAO = new AnimalDAO();
            feedingPlan.setAnimal(animalDAO.getAnimalById(resultSet.getString("animal_id")));
            return feedingPlan;
        } else {
            return null;
        }
    }

    public boolean updateFeedingPlan(FeedingPlan updatedFeedingPlan) throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "UPDATE FeedingPlan SET animal_id = ? WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, updatedFeedingPlan.getAnimal().getId());
        statement.setString(2, updatedFeedingPlan.getId());
        int rowsAffected = statement.executeUpdate();
        return rowsAffected > 0;
    }

    public boolean deleteFeedingPlan(String id) throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "DELETE FROM FeedingPlan WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        int rowsAffected = statement.executeUpdate();
        return rowsAffected > 0;
    }
}
