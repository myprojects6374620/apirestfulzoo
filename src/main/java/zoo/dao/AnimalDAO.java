package zoo.dao;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import zoo.model.Animal;
import zoo.util.DatabaseUtils;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class AnimalDAO {
    private Cache<String, Animal> animalCache = CacheBuilder.newBuilder()
            .maximumSize(100)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build();

    private Cache<String, List<Animal>> animalsCache = CacheBuilder.newBuilder()
            .maximumSize(100)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build();

    public void createAnimal(Animal animal) throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "INSERT INTO ANIMALS (id, name, species, age, sex, weight, health_status) VALUES (?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, animal.getId());
        statement.setString(2, animal.getName());
        statement.setString(3, animal.getSpecies());
        statement.setInt(4, animal.getAge());
        statement.setString(5, animal.getSex());
        statement.setDouble(6, animal.getWeight());
        statement.setString(7, animal.getHealthStatus());
        statement.executeUpdate();
    }

    public List<Animal> getAllAnimals(int page, int size) throws SQLException {
        String cacheKey = "page_" + page + "_size_" + size;
        List<Animal> cachedAnimals = animalsCache.getIfPresent(cacheKey);
        if (cachedAnimals != null) {
            return cachedAnimals;
        }

        List<Animal> animals = new ArrayList<>();
        Connection connection = DatabaseUtils.getConnection();
        String sql = "SELECT * FROM ANIMALS LIMIT ? OFFSET ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, size);
        statement.setInt(2, (page - 1) * size);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            Animal animal = new Animal();
            animal.setId(resultSet.getString("id"));
            animal.setName(resultSet.getString("name"));
            animal.setSpecies(resultSet.getString("species"));
            animal.setAge(resultSet.getInt("age"));
            animal.setSex(resultSet.getString("sex"));
            animal.setWeight(resultSet.getDouble("weight"));
            animal.setHealthStatus(resultSet.getString("health_status"));
            animals.add(animal);
        }
        animalsCache.put(cacheKey, animals);
        return animals;
    }

    public int getTotalAnimalsCount() throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "SELECT COUNT(*) FROM ANIMALS";
        PreparedStatement statement = connection.prepareStatement(sql);
        ResultSet resultSet = statement.executeQuery();
        if (resultSet.next()) {
            return resultSet.getInt(1);
        }
        return 0;
    }

    public Animal getAnimalById(String id) throws SQLException {
        Animal cachedAnimal = animalCache.getIfPresent(id);
        if (cachedAnimal != null) {
            return cachedAnimal;
        }

        Connection connection = DatabaseUtils.getConnection();
        String sql = "SELECT * FROM ANIMALS WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            Animal animal = new Animal();
            animal.setId(resultSet.getString("id"));
            animal.setName(resultSet.getString("name"));
            animal.setSpecies(resultSet.getString("species"));
            animal.setAge(resultSet.getInt("age"));
            animal.setSex(resultSet.getString("sex"));
            animal.setWeight(resultSet.getDouble("weight"));
            animal.setHealthStatus(resultSet.getString("health_status"));
            animalCache.put(id, animal);
            return animal;
        } else {
            return null;
        }
    }

    public boolean updateAnimal(String id, Animal updatedAnimal) throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "UPDATE ANIMALS SET name = ?, species = ?, age = ?, sex = ?, weight = ?, health_status = ? WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, updatedAnimal.getName());
        statement.setString(2, updatedAnimal.getSpecies());
        statement.setInt(3, updatedAnimal.getAge());
        statement.setString(4, updatedAnimal.getSex());
        statement.setDouble(5, updatedAnimal.getWeight());
        statement.setString(6, updatedAnimal.getHealthStatus());
        statement.setString(7, id);
        int rowsAffected = statement.executeUpdate();
        return rowsAffected > 0;
    }

    public boolean deleteAnimal(String id) throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "DELETE FROM ANIMALS WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        int rowsAffected = statement.executeUpdate();
        return rowsAffected > 0;
    }
}


/*Inicialización del Caché: El caché se inicializa con un
tamaño máximo de 100 elementos y una expiración de 10 minutos después de la escritura.

Método getAnimalById: Primero intenta obtener el animal
del caché. Si no está en el caché, lo busca en la base de
 datos y lo guarda en el caché antes de devolverlo.

Método updateAnimal: Después de

actualizar un animal en la base de datos, también
actualiza el caché con el nuevo valor.

Método deleteAnimal: Después de eliminar un animal de la
base de datos, invalida (elimina) el valor correspondiente del caché.
Con estas modificaciones, la clase AnimalDAO ahora utiliza
un caché en memoria para mejorar el rendimiento al reducir
la cantidad de accesos a la base de datos. Esto es especialmente
 útil para operaciones de lectura frecuentes como getAnimalById.*/