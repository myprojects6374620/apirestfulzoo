package zoo.dao;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import zoo.model.Food;
import zoo.model.Nutrition;
import zoo.util.DatabaseUtils;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class FoodDao {
    private Cache<String, Food> foodCache = CacheBuilder.newBuilder()
            .maximumSize(100)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build();

    private Cache<String, List<Food>> foodsCache = CacheBuilder.newBuilder()
            .maximumSize(100)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .build();

    public void createFood(Food food) throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "INSERT INTO Food (id, name, food_type, calories, protein, fat) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, food.getId());
        statement.setString(2, food.getName());
        statement.setString(3, food.getFoodType());
        Nutrition nutritionalValue = food.getNutritionalValue();
        statement.setInt(4, nutritionalValue.getCalories());
        statement.setDouble(5, nutritionalValue.getProtein());
        statement.setDouble(6, nutritionalValue.getFat());
        statement.executeUpdate();
    }

    public List<Food> getAllFoods(int page, int size) throws SQLException {
        String cacheKey = "page_" + page + "_size_" + size;
        List<Food> cachedFoods = foodsCache.getIfPresent(cacheKey);
        if (cachedFoods != null) {
            return cachedFoods;
        }

        List<Food> foods = new ArrayList<>();
        Connection connection = DatabaseUtils.getConnection();
        String sql = "SELECT * FROM Food LIMIT ? OFFSET ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, size);
        statement.setInt(2, (page - 1) * size);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            Food food = new Food();
            food.setId(resultSet.getString("id"));
            food.setName(resultSet.getString("name"));
            food.setFoodType(resultSet.getString("food_type"));
            Nutrition nutritionalValue = new Nutrition(
                    resultSet.getInt("calories"),
                    resultSet.getDouble("protein"),
                    resultSet.getDouble("fat")
            );
            food.setNutritionalValue(nutritionalValue);
            foods.add(food);
        }
        foodsCache.put(cacheKey, foods);
        return foods;
    }

    public List<Food> getAllFoods() throws SQLException {
        // Usa valores por defecto para la paginación
        int defaultPage = 1;
        int defaultSize = 10;
        return getAllFoods(defaultPage, defaultSize);
    }

    public Food getFoodById(String id) throws SQLException {
        Food cachedFood = foodCache.getIfPresent(id);
        if (cachedFood != null) {
            return cachedFood;
        }

        Connection connection = DatabaseUtils.getConnection();
        String sql = "SELECT * FROM Food WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        ResultSet resultSet = statement.executeQuery();

        if (resultSet.next()) {
            Food food = new Food();
            food.setId(resultSet.getString("id"));
            food.setName(resultSet.getString("name"));
            food.setFoodType(resultSet.getString("food_type"));
            Nutrition nutritionalValue = new Nutrition(
                    resultSet.getInt("calories"),
                    resultSet.getDouble("protein"),
                    resultSet.getDouble("fat")
            );
            food.setNutritionalValue(nutritionalValue);
            foodCache.put(id, food);
            return food;
        } else {
            return null;
        }
    }

    public boolean updateFood(String id, Food updatedFood) throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "UPDATE Food SET name = ?, food_type = ?, calories = ?, protein = ?, fat = ? WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, updatedFood.getName());
        statement.setString(2, updatedFood.getFoodType());
        Nutrition nutritionalValue = updatedFood.getNutritionalValue();
        statement.setInt(3, nutritionalValue.getCalories());
        statement.setDouble(4, nutritionalValue.getProtein());
        statement.setDouble(5, nutritionalValue.getFat());
        statement.setString(6, id);
        int rowsAffected = statement.executeUpdate();
        return rowsAffected > 0;
    }

    public boolean deleteFood(String id) throws SQLException {
        Connection connection = DatabaseUtils.getConnection();
        String sql = "DELETE FROM Food WHERE id = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, id);
        int rowsAffected = statement.executeUpdate();
        return rowsAffected > 0;
    }
}
