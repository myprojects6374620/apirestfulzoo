package zoo;
import zoo.model.Animal;

import java.util.List;

public class AnimalResponse {
    private Animal animal;
    private List<Link> links;

    public AnimalResponse() {
    }

    public AnimalResponse(Animal animal, List<Link> links) {
        this.animal = animal;
        this.links = links;
    }

    public Animal getAnimal() {
        return animal;
    }

    public void setAnimal(Animal animal) {
        this.animal = animal;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
