-- Tabla Animal
CREATE TABLE ANIMALS (
    id VARCHAR(36) PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    species VARCHAR(255) NOT NULL,
    age INT NOT NULL,
    sex VARCHAR(10) NOT NULL,
    weight DOUBLE NOT NULL,
    health_status VARCHAR(255) NOT NULL
);

-- Tabla AnimalProfile
CREATE TABLE AnimalProfile (
    id VARCHAR(36) PRIMARY KEY,
    animal_id VARCHAR(36) NOT NULL,
    dietary_needs VARCHAR(255),
    medical_history VARCHAR(255),
    FOREIGN KEY (animal_id) REFERENCES ANIMALS(id)
);

-- Tabla Food
CREATE TABLE Food (
    id VARCHAR(36) PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    food_type VARCHAR(255) NOT NULL,
    calories INT NOT NULL,
    protein DOUBLE NOT NULL,
    fat DOUBLE NOT NULL
);

-- Tabla FeedingPlan
CREATE TABLE FeedingPlan (
    id VARCHAR(36) PRIMARY KEY,
    animal_id VARCHAR(36) NOT NULL,
    FOREIGN KEY (animal_id) REFERENCES ANIMALS(id)
);

-- Tabla FeedingPlanMeal
CREATE TABLE FeedingPlanMeal (
    feeding_plan_id VARCHAR(36) NOT NULL,
    food_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (feeding_plan_id, food_id),
    FOREIGN KEY (feeding_plan_id) REFERENCES FeedingPlan(id),
    FOREIGN KEY (food_id) REFERENCES Food(id)
);